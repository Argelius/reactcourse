import React, { useState, useEffect } from "react";

function App() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState(window.localStorage.getItem("name") || "");

  useEffect(() => {
    console.log("updating local storage");
    window.localStorage.setItem("name", name);
  }, [name]);

  function handleChange(e) {
    setName(e.target.value);
  }

  return (
    <div style={{ padding: '2rem' }}>
      <input
        placeholder="What's your name?"
        value={name}
        onChange={handleChange}
      />
      <p>Hello, {name || "Stranger"}!</p>

      <div>
        <button onClick={() => setCount(count + 1)}>I have been clicked {count} times.</button>
      </div>
    </div>
  );
}

export default App;
