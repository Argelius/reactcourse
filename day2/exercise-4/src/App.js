import React, { useState, useRef } from "react";

function validatePassword(password) {
  if (password.length < 8) {
    return false;
  }

  if (!/[0-9]/.test(password)) {
    return false;
  }

  if (!/[a-zA-Z]/.test(password)) {
    return false;
  }

  return true;
}

function App() {
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const ref = useRef(null);

  function handleChange(e) {
    setPassword(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();

    if(!validatePassword(password)) {
      ref.current.focus();
      setError(true);
    } else {
      setError(false);
    }
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          type="password"
          value={password}
          onChange={handleChange}
          ref={ref}
        />
        <button type="submit">Validate password</button>
      </form>
      {error && <p style={{ color: "red" }}>Invalid password. Password must be 8 or more characters long and contain both numbers and alphabetical characters.</p>}
    </>
  );
}

export default App;
