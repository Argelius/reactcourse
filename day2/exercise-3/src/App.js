import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import axios from "axios";

function useFruits() {
  const [fruits, setFruits] = useState(null);

  useEffect(() => {
    axios.get("/fruits.json")
      .then((response) => {
        setFruits(response.data.fruits);
      });
  }, []);

  return fruits;
}

function Master() {
  const fruits = useFruits();

  if (fruits === null) {
    return <p>Loading fruits...</p>;
  }

  return (
    <ul>
      {fruits.map((fruit) => {
        return (
          <li key={fruit.name}>
            <NavLink activeStyle={{ fontWeight: 'bold' }} to={"/" + fruit.name}>
              {fruit.name}
            </NavLink>
          </li>
        );
      })}
    </ul>
  );
}

function Detail({ match }) {
  const fruits = useFruits();
  const name = match.params.name;

  useEffect(() => {
    document.title = name;
  }, [name]);

  if (fruits === null) {
    return <p>Loading fruit...</p>;
  }

  const fruit = fruits.find(x => x.name === name);

  return (
    <>
      <h2>{fruit.name}</h2>
      <p>{fruit.description}</p>
      <img src={fruit.photoUrl} alt={fruit.name} />
    </>
  );
}

function App() {
  return (
    <Router>
      <div style={{ padding: '2rem' }}>
        <h1>Fruits</h1>
        <Route path="/" component={Master} />
        <Route path="/:name" component={Detail} />
      </div>
    </Router>
  );
}

export default App;
