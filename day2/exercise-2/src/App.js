import { BrowserRouter as Router, Route } from "react-router-dom";

import Header from "./Header";
import Home from "./Home";
import About from "./About";
import Page from "./Page";

function App() {
  return (
    <div>
      <Router>
        <Header />

        <main style={{ padding: '2rem' }}>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/page" component={Page} />
        </main>
      </Router>
    </div>
  );
}

export default App;
