import React from "react";
import { Link, NavLink } from "react-router-dom";

import styles from "./Header.module.css";

export default function Header() {
  return (
    <header className={styles.container}>

      <h1>
        <Link className={styles.link} to="/">My page</Link>
      </h1>
      <nav>
        <ul className={styles.navlist}>
          <li className={styles.navitem}>
            <NavLink
              to="/about"
              className={styles.link}
              activeClassName={styles.active}
            >
              About
            </NavLink>
          </li>
          <li className={styles.navitem}>
            <NavLink
              to="/page"
              className={styles.link}
              activeClassName={styles.active}
            >
              Page
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}
