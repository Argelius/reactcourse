
import Square from "./Square";

function App() {
  return (
    <div style={{ padding: 20 }}>
      <Square />
    </div>
  );
}

export default App;
