import { BrowserRouter as Router, Route } from "react-router-dom";

import PageA from "./PageA";
import PageB from "./PageB";

function App() {
  return (
    <Router>
      <Route exact path="/" component={PageA} />
      <Route path="/page-b" component={PageB} />
    </Router>
  );
}

export default App;
