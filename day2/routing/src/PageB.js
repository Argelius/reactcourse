import React from "react";
import { Link } from "react-router-dom";

export default function PageB() {
  return (
    <>
      <h1>Page B</h1>
      <hr />
      <p>
        Welcome to Page B! If you want to go back to Page A, please click <Link to="/">this link</Link>.
      </p>
    </>
  );
}
