import React from "react";
import { Link } from "react-router-dom";

export default function PageA() {
  return (
    <>
      <h1>Page A</h1>
      <hr />
      <p>
        This is page A. Click <Link to="/page-b">this link</Link> to go to Page B.
      </p>
    </>
  );
}
