import React from "react";

import MyButton from "./MyButton";
import AnotherButton from "./AnotherButton";

function App() {
  return (
    <div style={{ padding: 20 }}>
      <MyButton>
        This is a button
      </MyButton>
      <AnotherButton>
        This is another button
      </AnotherButton>
    </div>
  );
}

export default App;
