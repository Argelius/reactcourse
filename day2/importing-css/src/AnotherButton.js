import React from "react";

import styles from "./AnotherButton.module.css";

export default function AnotherButton({ children }) {
  return (
    <button className={styles.container}>{children}</button>
  );
}
