import React from "react";

import "./MyButton.css";

export default function MyButton({ children }) {
  return (
    <button className="MyButton-container">
      {children}
    </button>
  );
}
