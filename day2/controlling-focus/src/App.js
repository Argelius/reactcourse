import React, { useState, useRef, useEffect } from "react";

function App() {
  const ref = useRef(null);

  function handleClick() {
    ref.current.focus();
  }

  return (
    <>
      <h1>Controlling focus</h1>
      <input
        type="text"
        ref={ref}
      />
      <button onClick={handleClick}>Focus!</button>
    </>
  );
}

export default App;
