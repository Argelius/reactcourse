import './App.css';

const NAMES = ["Bob", "Alice", "Eve", "Mallory"];

function App() {

  return (
    <>
      <h1>List rendering</h1>
      <ul>
        {NAMES.map((name) => <li key={name}>{name}</li>)}
      </ul>
    </>

  );
}

export default App;
