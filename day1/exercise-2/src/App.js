import logo from './logo.svg';
import './App.css';

function Calc({ a, b, operator }) {
  let result = null;
  let operatorChar = null;

  switch (operator) {
    case "add":
      result = a + b;
      operatorChar = "+";
      break;
    case "mul":
      result = a * b;
      operatorChar = "*";
      break;
    case "sub":
      result = a - b;
      operatorChar = "-";
      break;
    case "div":
      result = a / b;
      operatorChar = "/";
      break;
    default:
      result = 0;
      operatorChar = "unknown operator";
  }

  return <p>{a} {operatorChar} {b} = {result}</p>;
}

function App() {
  return (
    <>
      <h1>Calculations</h1>
      <Calc a={10} b={20} operator="add" />
      <Calc a={10} b={20} operator="mul" />
      <Calc a={10} b={20} operator="sub" />
      <Calc a={10} b={20} operator="div" />
    </>
  );
}

export default App;
