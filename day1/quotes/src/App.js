import React, { useState, useEffect } from "react";
import axios from "axios";

function App() {
  const [quote, setQuote] = useState(null);

  function fetchRandomQuote() {
    axios.get("https://api.quotable.io/random")
      .then((response) => {
        setQuote(response.data);
      });
  }

  useEffect(() => {
    fetchRandomQuote();
  }, []);

  if (!quote) {
    return <p>Fetching quote...</p>;
  }

  return (
    <>
      <figure>
        <blockquote>
          <p>{quote.content}</p>
        </blockquote>
        <figcaption>{quote.author}</figcaption>
      </figure>

      <div>
        <button onClick={fetchRandomQuote}>Fetch another quote</button>
      </div>
    </>
  );
}

export default App;
