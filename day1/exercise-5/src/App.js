import React, { useState, useEffect } from "react";
import axios from "axios";

function App() {
  const [page, setPage] = useState(1);
  const [beers, setBeers] = useState([]);
  
  useEffect(() => {
    fetchBeers();
  }, []);

  function fetchBeers() {
    axios.get(`https://api.punkapi.com/v2/beers?page=${page}&per_page=5`)
      .then((response) => {
        setBeers([...beers, ...response.data]);
        setPage(page + 1);
      });
  }

  if (beers.length === 0) {
    return <p>Fetching beers...</p>;
  }

  return (
    <>
      <h1>Beer</h1>
      <ul>
        {beers.map((beer) => (
          <li>{beer.name}</li>
        ))}
      </ul>
      <button onClick={fetchBeers}>Fetch more!</button>
    </>
  );
}

export default App;
