import logo from './logo.svg';
import './App.css';

function Adder({ a, b }) {
  return (
    <p>{a} + {b} = {a + b}</p>
  );
}

function App() {
  return (
    <>
      <h1>Simple adder</h1>

      <Adder a={10} b={20} />
      <Adder a={2} b={13} />
      <Adder a={42} b={17} />
      <Adder a={100} b={20} />
    </>
  );
}

export default App;
