import React, { useState } from "react";
import './App.css';

function App() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  function handleUsernameChange(e) {
    setUsername(e.target.value);
  }

  function handlePasswordChange(e) {
    setPassword(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (username === "andreas" && password === "pass123") {
      setError(false);
      setSuccess(true);
    } else {
      setError(true);
      setSuccess(false);
    }
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={username}
          onChange={handleUsernameChange}
        />
        <input
          type="password"
          value={password}
          onChange={handlePasswordChange}
        />
        <button type="submit">Sign in</button>
      </form>
      {success && <p style={{ color: "green" }}>You have logged in!</p>}
      {error && <p style={{ color: "red" }}>Username or password was incorrect! Please try again!</p>}
    </>
  );
}

export default App;
