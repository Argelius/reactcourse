import React, { useState } from "react";

function App() {
  const [name, setName] = useState("Andreas");

  function handleNameChange(e) {
    setName(e.target.value);
  }

  return (
    <>
      <input
        type="text"
        placeholder="Please enter your name"
        value={name}
        onChange={handleNameChange}
      />
      <p>Hello, {name}!</p>
    </>
  );
}

export default App;
