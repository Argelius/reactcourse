import React, { useState, useEffect } from 'react';

function TabA() {
  useEffect(() => {
    console.log("Tab A was created");

    return () => {
      console.log("Tab A was destroyed");
    };
  }, []);

  return (
    <p>This is the content of the first tab.</p>
  );
}

function TabB() {
  useEffect(() => {
    console.log("Tab B was created");

    return () => {
      console.log("Tab B was destroyed");
    };
  }, []);


  return (
    <p>This is the content of the second tab.</p>
  );
}

function TabC() {
  useEffect(() => {
    console.log("Tab C was created");

    return () => {
      console.log("Tab C was destroyed");
    };
  }, []);

  return (
    <p>This is the content of the third tab.</p>
  );
}

function App() {
  const [activeTab, setActiveTab] = useState(0);

  let content = null;

  if (activeTab === 0) {
    content = <TabA />;
  } else if (activeTab === 1) {
    content = <TabB />;
  } else if (activeTab === 2) {
    content = <TabC />;
  }

  return (
    <>
      <a href="#" onClick={() => setActiveTab(0)}>Page 1</a>
      <a href="#" onClick={() => setActiveTab(1)}>Page 2</a>
      <a href="#" onClick={() => setActiveTab(2)}>Page 3</a>
      {content}
    </>
  );
}

export default App;
